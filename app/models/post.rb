class Post < ActiveRecord::Base
    has_many :comments, dependent: :destroy
    #validates :title, :presence => true
    #validates :body, :presence => true
    validates_presence_of :title
    validates_presence_of :body
end
